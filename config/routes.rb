Rails.application.routes.draw do
  devise_for :users
  devise_scope :user do
    get "users/current", to: "sessions#show"
  end
  get "/users/search" => "users#search"

  resources :cubes
  get "/draft/:id" => "cubes#draft"

  resources :cards

  resources :decks, only: %i[show edit]

  resources :follows, only: %i[index show create destroy]

  root to: "application#index"
end
