class AddUserRefToCubes < ActiveRecord::Migration[5.2]
  def change
    add_reference :cubes, :user, foreign_key: true
  end
end
