<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Introduction](#introduction)
- [Types of Contributions](#types-of-contributions)
  - [Adding a New Feature](#adding-a-new-feature)
  - [Updating an Existing Feature's Behavior](#updating-an-existing-features-behavior)
  - [Performance Tweaks](#performance-tweaks)
  - [Issue or Bug Fixing](#issue-or-bug-fixing)
  - [What About Feature Suggestions?](#what-about-feature-suggestions)
- [Ground Rules](#ground-rules)
  - [Browser Support](#browser-support)
  - [Spelling](#spelling)
  - [Branch Sizes and Scope](#branch-sizes-and-scope)
  - [Etiquette](#etiquette)
  - [Test Coverage](#test-coverage)
- [Getting Started](#getting-started)
  - [Setting up Your Local Environment](#setting-up-your-local-environment)
  - [Add Yourself to the ReadMe!](#add-yourself-to-the-readme)
  - [Commenting Policy and Documentation](#commenting-policy-and-documentation)
  - [Testing Before Making a PR](#testing-before-making-a-pr)
  - [`.gitignore` changes](#gitignore-changes)
- [If You're New](#if-youre-new)
  - [Fork the Repo](#fork-the-repo)
  - [Create a Pull Request](#create-a-pull-request)
  - [The Review Process](#the-review-process)
  - [Resources](#resources)
- [Suggest a Feature](#suggest-a-feature)
  - [View our Project Charter](#view-our-project-charter)
  - [Add it to the Backlog](#add-it-to-the-backlog)
- [Talk to Us on Gitter!](#talk-to-us-on-gitter)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

---

# Introduction

We're glad you're here! Playing Cube and Magic in general is about community and your efforts on this application contribute to that community as much as the players who enjoy the fruits of our labor. The more we contribute to making this a fantastic tool that we can all enjoy, the better we can grow this community and bring people into our fantastic hobby.

By contributing along these guidelines, we maintain a consistency between our pull requests. This makes it easy for me (Daniel Jauch) to make sure that your changes get merged, and easy for the continuous integration tools to keep things running smoothly. Not to mention the obvious knock-on effects that the users benefit from quality programming.

# Types of Contributions

For the purposes of this work, there are four major types of contributions that we're looking for in this application. If you're picking one of these off of the backlog, follow the [Getting Started](#getting-started) guide below, then set up a branch with the prefix listed.

## Adding a New Feature

**Branch prefix:** `feature/`

This is used for anything that adds a new object, property, or method to the application.

## Updating an Existing Feature's Behavior

**Branch prefix:** `update/`

This is used for any time that the objects, properties, or methods **already a part** of the application are being changed. In most cases, this is not something I'll be asking for help with as it will likely come back to the project charter on implementation before starting work, though once the work has begun, I appreciate the help.

> **Note:** You'll also want to take a look at the comment about this branch type under [Test Coverage](#test-coverage) as well.

## Performance Tweaks

**Branch prefix:** `performance/`

This is for those people who really like finding micro-adjustments to save all the milliseconds we can.

One note on this: Readability and convention come first. Because this is an open source project, it's very important that methods are very transparent in the way that they operate and the purpose for which they operate. Rubocop will likely keep these things in check, but let's keep it as transparent as possible.

## Issue or Bug Fixing

**Branch prefix:** `bugfix/` or `hotfix/`

This is for fixing an unexpected or emergent behavior that is unintended.

To define the difference of a bug fix vs. a hot fix for this project, a hot fix is only used in the following circumstances:

- Runtime errors
- Core pieces of the application not operating at 100%
- Server downtime due to the code
- A discovered vulnerability in the application (more about this in [Etiquette](#etiquette))

## What About Feature Suggestions?

A very important tennant of the development of this application (in more detail under [Ground Rules](#ground-rules)) is that the users are the true owners of the application. It's for them, so they decide the priority.

Please add a feature suggestion to the backlog and let the community decide on priority.

**To be clear**, I'm in **no way** discouraging you from working on new features that you are passionate about. However, pull requests will be merged in priority of the backlog, so the lower your request is on the list, the more likely you will need to deal with merge conflicts. All the more reason to help with the existing backlog.

# Ground Rules

## Browser Support

The basic guideline is what is present in the standard support library, but here's the table:

| IE | Edge | Chrome | FireFox | Safari | Opera |
| :-: | :-: | :-: | :-: | :-: | :-: | :-: |
| 11+ | 17+ | 67+ | 61+ | 11.1+ | 53+ |

## Spelling

For the purposes of class names, method names, body copy, or any other text in the repository, this application uses the American English spelling for everything. The rationale behind this is because Magic: the Gathering is a United States-based company that uses entirely American English for the card text, website text, API variable names, and everything else. Expect PR comments if you're spelling _color_ like _colour_.

Oxford Dictionaries has a great guide [here](https://en.oxforddictionaries.com/spelling/british-and-spelling) if you're unsure.

## Branch Sizes and Scope

Please work to change as few files as possible. If you notice a typo or an issue as you're looking through the files, make a separate PR for that one-file change. Don't include these small changes in your feature work. This is to help with potential merge conflicts becoming a hassle with important feature work.

When in doubt, break it up. It's absolutely acceptable to make a PR with a description to the effect of...

> This is a fairly benign change, but it sets up the basis for feature \___ in the backlog. It currently contains pending tests for \___ which will be used in an upcoming branch named `feature/___`

In this case, maintain some obvious naming scheme in folder format like so:

1. Build base method in super class: `feature/new-feature-name/method-setup`
2. Inherit class and ensure that validations still work as expected: `feature/new-feature-name/subclass-and-tests`
3. Override superclass for unique behavior: `feature/new-feature-name/method-override`

Then, include descriptions of which pull requests rely on which others.

Though this seems messy, it is much easier for updating production environments with granular, well-considered steps.

## Etiquette

At some point, every one of us did our first PR. Be respectful and considerate when leaving your comments. When in doubt, employ the [ASK method](https://medium.com/@gregthewhite/giving-actionable-specific-and-kind-feedback-6fc83eb04a65) of feedback.

Example:

| Not helpful comment | Helpful alternative |
| --- | --- |
| This doesn't work. | I noticed in IE 11, this flex property isn't respecting the boundaries of the container, try ___ as an alternative. |
| I don't like this color. | Based on the styleguide, I'm not sure this fits our accessibility guidelines. Would you mind darkening the color? Just 10% darker seemed like it greatly increased the visibility. |
| Ugh, I hate arrow syntax. | Would you mind making this a code block instead? I think this could be more readable that way. |

If you find a security vulnerability, do NOT open an issue. Email [mtgcubedraft.contact@gmail.com](mailto:mtgcubedraft.contact@gmail.com) instead.

## Test Coverage

This application uses [RSpec Rails 3.8](http://rspec.info/documentation/3.8/rspec-rails/) for tests.

Unless your features have tests and those tests check the passing and failing state, it will not get accepted. This isn't my personal fixation on tests or an attempt to make your development difficult. The purpose of this policy is for maintenance. This helps define the separation between a `feature` and an `update`.

A new contributor may also use the tests to understand the intention of a particular method or overarching feature. Having tests requires us to describe the _what_ and **why** of any piece of the application.

# Getting Started

## Setting up Your Local Environment

1. Clone the repository
2. Install the bundle
3. Set up the local database
4. Work

## Add Yourself to the ReadMe!

We're all about you getting credit for your hard work. Please include your name under the correct category (Developer vs. Designer), linked to your portfolio. No social links please unless it's your only source of your work. Once people are on your portfolio, people should be able to get to everywhere else. We want this to be about promoting your work, not your "brand".

One link per person, please.

## Commenting Policy and Documentation

This one may be a sticking point for some people: please avoid comments. If your code is vague enough that it can't be understood without a comment, it may need refactoring.

| Unwanted comments | Reasoning |
| --- | --- |
| Class docs | There are very few class names in the application and the responsibilities should be clear from their names. If they aren't and you'd like to make a change, it will be worth revisiting the project charter. |
| Method docs | The method name should be clear and concise. If you see a potential conflict or vague method name that requires refactoring, this should be done as a separate PR. Do your work with the concern in your description, and we can revisit the work or provide a suggestion. |
| To do notes* | This is what the backlog is for. If your feature is incomplete, there is either more work you should do on your branch or you need to do a multi-branch change. Revisit [Branch Sizes and Scope](#branch-sizes-and-scope) |
| Directives for other work | Firstly, that's not your work, so best not to comment unless you're a reviewer in a PR. Second, comments in the backlog are probably a better place for this. |
| Explanations for a particular implementation | Let your work speak for itself. If there is a concern from reviewers, that's what the review comment system is for. Try not to bloat the repo. |

*: If you would like to make personal reminders, use the template in `personal-notes-example.md` To make a new file `personal-notes.md` and keep your notes in there. Go wild.

## Testing Before Making a PR

To save us on CI load, please run the following tests before creating your pull request. Once you have made a pull request any subsequent commit will rerun the integration build, so avoiding this would be great.

> **Note:** Skipping your CI build in your commit message (by including `--skip-ci`) is fine for commits done after the PR is build, keeping in mind that the merge of your branch requires that the last push includes a successful build.

## `.gitignore` changes

Make a separate PR with specifically that change. If it's your code editor configurations, hopefully the `.editorconfig` reduces the need for this, but there are always exceptions.

# If You're New

## Fork the Repo

Go to the [repository homepage](https://gitlab.com/danieljauch/mtg-cube-app) and click the "Fork" button in the top left. This will make a copy of the repository on your GitLab profile.

Clone the repo on your local machine and use the setup instructions in the [ReadMe](https://gitlab.com/danieljauch/mtg-cube-app/blob/master/README.md).

## Create a Pull Request

After commiting and pushing your work, create a pull request against the base repository's `master` branch. This is the development branch.

## The Review Process

If members from the community or I have any issues with your pull request, we'll leave a comment which is removed after the comment is addressed. Then, anyone can approve your review. After one member of the community and I approve your review, I'll merge it into the staging environment, a group of testers will ensure the work is complete to satisfaction, and merge it into production.

## Resources

**Working on your first Pull Request?** You can learn how from this *free* series [How to Contribute to an Open Source Project on GitHub](https://egghead.io/series/how-to-contribute-to-an-open-source-project-on-github)

Another great resource is [First Timers Only](http://www.firsttimersonly.com/).

# Suggest a Feature

## View our Project Charter

Take a moment to read our [Project Charter](https://docs.google.com/document/d/1Cwr_gMpwXVmwHteTeBrSrUsOidRkVDHuWg3gPJ81keI/edit?usp=sharing) to ensure that your request is something that fits with the project goals, has a specific purpose for the users, and is a beneficial addition to the application.

## Add it to the Backlog

The true list of stories is managed privately to maintain consistency, but start by [adding an issue](https://gitlab.com/danieljauch/mtg-cube-app/issues/new) with the following template:

- Type of work: [feature, update, bug, hotfix, performance]
- Targetted platforms: [All, Windows, Mac, Linux (please be specific if this is about a bug or hotfix)]
- (If a bug or hotfix) Effected browsers: [IE, Edge, FireFox, Chrome, Opera, Safari, please list version number if possible]
- (If a feature or update) Benefit to the users: [If you're familiar with [Agile user stories](https://www.atlassian.com/agile/project-management/user-stories), this is the place for them]
- User acceptance criteria: [Provide a list of all things that would be verifiable in order for the work to be considered "complete"]
- (Optional) Other details:

# Talk to Us on Gitter!

Get connected on [our Gitter](https://gitter.im/cubedraft) and feel free to ask as many questions about development as you like! The more information you get about what it is that we're looking for, the more likely you'll be able to make an ideal contribution.
