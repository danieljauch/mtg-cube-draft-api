# Magic: The Gathering Cube (API)

## Involvement

Developers:

- [Daniel Jauch](https://danieljauch.com/)

Designers:

- Shakira Jensen

## Development setup

```
cp example.env .env
bundle
bundle exec rails db:setup
bundle exec rails s
```

## Testing CI checks before committing

```
bundle audit
bundle exec rubocop
bundle exec brakeman
bundle exec rspec
```

## Deployment

| Environment | Branch              | Deployment URL                                                                   | Current Version |
| ----------- | ------------------- | -------------------------------------------------------------------------------- | --------------- |
| Staging | `master`            | [mtgcubedraftapi-staging.herokuapp.com](https://mtgcubedraftapi-staging.herokuapp.com) | 0.0.1           |
| Production  | `deploy/production` | [mtgcubedraftapi-production.herokuapp.com](https://mtgcubedraftapi-production.herokuapp.com) | 0.0.1           |

---

### Resources

This app uses:
- The [MTG Card Search API](https://docs.magicthegathering.io) ([Ruby SDK](https://github.com/MagicTheGathering/mtg-sdk-ruby)).
- The [TCG Player Pricing API](http://developer.tcgplayer.com)

[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](http://makeapullrequest.com)
