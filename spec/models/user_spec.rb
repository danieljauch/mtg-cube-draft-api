require "rails_helper"
require "devise"

RSpec.describe User, type: :model do
  subject { FactoryBot.create(:user) }

  it "has a valid factory" do
    expect(subject).not_to be_nil
  end

  describe "when creating a username" do
    # Character count
    context "with less than 8 characters in their username" do
      let(:user) { FactoryBot.build(:user, :with_less_than_8_characters) }

      it "has an invalid username" do
        expect { user.save! }.to raise_error(ActiveRecord::RecordInvalid)
      end
    end

    context "with more than 24 characters in their username" do
      let(:user) { FactoryBot.build(:user, :with_more_than_24_characters) }

      it "has an invalid username" do
        expect { user.save! }.to raise_error(ActiveRecord::RecordInvalid)
      end
    end

    context "with between 8 and 24 characters in their username" do
      let(:user) { FactoryBot.build(:user, :with_between_8_and_24_characters) }

      it "has a valid username" do
        expect { user.save! }.not_to raise_error
      end
    end

    # Characters
    context "with an invalid symbol" do
      let(:user) { FactoryBot.build(:user, :with_an_invalid_symbol) }

      it "has an invalid username" do
        expect { user.save! }.to raise_error(ActiveRecord::RecordInvalid)
      end
    end

    context "with a number" do
      let(:user) { FactoryBot.build(:user, :with_a_number) }

      it "has a valid username" do
        expect { user.save! }.not_to raise_error
      end
    end

    context "with all valid characters" do
      let(:user) { FactoryBot.build(:user, :with_all_valid_characters) }

      it "has a valid username" do
        expect { user.save! }.not_to raise_error
      end
    end

    # Uniqueness
    context "with an existing username" do
      let!(:user_original) { FactoryBot.create(:user, :example_existing_username) }
      let(:user) { FactoryBot.build(:user, :check_existing_username) }

      it "has an invalid username" do
        expect { user.save! }.to raise_error(ActiveRecord::RecordInvalid)
      end
    end

    context "with a new, unique username" do
      let(:user) { FactoryBot.build(:user, :with_unique_username) }

      it "has a valid username" do
        expect { user.save! }.not_to raise_error
      end
    end
  end

  describe "when choosing an email" do
    context "with an empty email" do
      let(:user) { FactoryBot.build(:user, :with_empty_email) }

      it "has an invalid email" do
        expect { user.save! }.to raise_error(ActiveRecord::RecordInvalid)
      end
    end

    context "with a non-unique email" do
      let!(:user1) { FactoryBot.create(:user) }
      let(:user2) { FactoryBot.build(:user, :with_between_8_and_24_characters) }

      it "has an invalid email" do
        expect { user2.save! }.to raise_error(ActiveRecord::RecordInvalid)
      end
    end

    context "with a technically unique email due to casing" do
      let!(:user1) { FactoryBot.create(:user) }
      let(:user2) do
        FactoryBot.build(:user, :with_between_8_and_24_characters, :with_cased_unique_email)
      end

      it "has an invalid email" do
        expect { user2.save! }.to raise_error(ActiveRecord::RecordInvalid)
      end
    end

    context "with a valid email" do
      let(:user) { FactoryBot.build(:user) }

      it "has a valid email" do
        expect { user.save! }.not_to raise_error
      end
    end
  end

  describe "when creating a password" do
    # Character count
    context "with less than 8 characters in their password" do
      let(:user) { FactoryBot.build(:user, :with_short_password) }

      it "has an invalid password" do
        expect { user.save! }.to raise_error(ActiveRecord::RecordInvalid)
      end
    end

    context "with more than 24 characters in their password" do
      let(:user) { FactoryBot.build(:user, :with_long_password) }

      it "has an invalid password" do
        expect { user.save! }.to raise_error(ActiveRecord::RecordInvalid)
      end
    end

    # Characters
    context "without an uppercase letter" do
      let(:user) { FactoryBot.build(:user, :without_cap) }

      it "has an invalid password" do
        expect { user.save! }.to raise_error(ActiveRecord::RecordInvalid)
      end
    end

    context "without a lowercase letter" do
      let(:user) { FactoryBot.build(:user, :without_lower) }

      it "has an invalid password" do
        expect { user.save! }.to raise_error(ActiveRecord::RecordInvalid)
      end
    end

    context "without a number" do
      let(:user) { FactoryBot.build(:user, :without_num) }

      it "has an invalid password" do
        expect { user.save! }.to raise_error(ActiveRecord::RecordInvalid)
      end
    end

    context "without a special character" do
      let(:user) { FactoryBot.build(:user, :without_char) }

      it "has an invalid password" do
        expect { user.save! }.to raise_error(ActiveRecord::RecordInvalid)
      end
    end

    context "with a valid password" do
      let(:user) { FactoryBot.build(:user) }

      it "has a valid password" do
        expect { user.save! }.not_to raise_error
      end
    end
  end

  describe "#search" do
    let!(:user1) do
      FactoryBot.build(
        :user,
        username: "aec10000",
        email: "test@one.com"
      )
    end
    let!(:user2) do
      FactoryBot.build(
        :user,
        username: "cde10000",
        email: "test@two.com"
      )
    end
    let!(:user3) do
      FactoryBot.build(
        :user,
        username: "efg10000",
        email: "test@three.net"
      )
    end

    context "with no query" do
      let(:query) { "" }

      before do
        user1.save!
        user2.save!
        user3.save!
      end

      it "returns all users" do
        expect(User.search(query)).to match_array [user1, user2, user3]
      end
    end

    context "with a query" do
      before do
        user1.save!
        user2.save!
        user3.save!
      end

      context "that satisfies only usernames" do
        let(:query) { "10000" }

        it "returns appropriate users" do
          expect(User.search(query)).to match_array [user1, user2, user3]
        end
      end

      context "that satisfies only emails" do
        let(:query) { "." }

        it "returns appropriate users" do
          expect(User.search(query)).to match_array [user1, user2, user3]
        end
      end

      context "that satisfies only some usernames" do
        let(:query) { "c" }

        it "returns appropriate users" do
          expect(User.search(query)).to match_array [user1, user2]
        end
      end

      context "that satisfies only some emails" do
        let(:query) { ".com" }

        it "returns appropriate users" do
          expect(User.search(query)).to match_array [user1, user2]
        end
      end
    end

    describe "testing limit" do
      before do
        user1.save!
        user2.save!
        user3.save!
      end

      let(:query) { "e" }

      context "with no limit" do
        it "returns up to 10 (default limit) users" do
          expect(User.search(query).count).to eq 3
        end
      end

      context "with a limit" do
        let(:limit) { 1 }

        it "returns up to the limit" do
          expect(User.search(query, user1, limit).count).to eq 1
        end
      end
    end

    describe "testing current user" do
      before do
        user1.save!
        user2.save!
        user3.save!
      end

      let(:query) { "e" }

      context "with no current user" do
        it "returns appropriate users" do
          expect(User.search(query)).to match_array [user1, user2, user3]
        end
      end

      context "with a current user" do
        let(:limit) { 10 }

        before { login_as(user1) }

        it "returns all appropriate users except self" do
          expect(User.search(query, user1, limit)).to match_array [user2, user3]
        end
      end
    end
  end
end
