require "rails_helper"

RSpec.describe UserPolicy do
  subject { UserPolicy }

  context "when viewing another user" do
    xit "follows policy" do
      # :search? expect(subject).to permit()
      # :show? expect(subject).to permit()
      # user:username expect(subject).to permit()
      # user:email expect(subject).not_to permit()
      # user:cubes expect(subject).not_to permit()
      # user:decks expect(subject).not_to permit()
      # user:cards expect(subject).not_to permit()
      # user:follows expect(subject).not_to permit()
    end
  end

  context "when viewing yourself" do
    before do
      user.save!
      login_as(user)
    end

    xit "follows policy" do
      # :search? expect(subject).to permit()
      # :show? expect(subject).to permit()
      # user:username expect(subject).to permit()
      # user:email expect(subject).to permit()
      # user:cubes expect(subject).to permit()
      # user:decks expect(subject).to permit()
      # user:cards expect(subject).to permit()
      # user:follows expect(subject).to permit()
    end
  end
end
