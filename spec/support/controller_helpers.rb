module ControllerHelpers
  def sign_in_user(user = FactoryBot.create(:user))
    sign_in_user_as user
  end

  def sign_in_user_as(user = nil)
    if user.nil?
      sign_in_user_fail
    else
      sign_in_user_success user
    end
  end

  def sign_in_user_fail
    allow(request.env["warden"]).to receive(:authenticate!).and_throw(:warden, scope: :user)
    allow(controller).to receive(:current_user).and_return(nil)
  end

  def sign_in_user_success(user)
    allow(request.env["warden"]).to receive(:authenticate!).and_return(user)
    allow(controller).to receive(:current_user).and_return(user)
  end
end
