require "rails_helper"

RSpec.describe FollowsController, type: :controller do
  describe "GET #index" do
    it "responds with 200 OK" do
      get :index
      expect(response.status).to eq 200
    end
  end

  describe "POST #create" do
    context "with one user" do
      let(:user1) { FactoryBot.create(:user) }

      before { sign_in_user(user1) }

      it "doesn't follow self" do
        expect do
          post :create, params: { destination_id: user1.to_param }
        end.not_to change(Follow, :count)
      end
    end

    context "with multiple users" do
      let(:user1) { FactoryBot.create(:user) }
      let(:user2) { FactoryBot.create(:user, :with_unique_params) }

      before { sign_in_user(user1) }

      it "creates new follow" do
        expect do
          post :create, params: { destination_id: user2.to_param }
        end.to change(Follow, :count).by(1)
      end

      it "doesn't create duplicate follow" do
        post :create, params: { destination_id: user2.to_param }

        expect do
          post :create, params: { destination_id: user2.to_param }
        end.not_to change(Follow, :count)
      end
    end
  end

  describe "GET #show" do
    let(:user1) { FactoryBot.create(:user) }
    let(:user2) { FactoryBot.create(:user, :with_unique_params) }

    before do
      post :create, params: { destination_id: user2.to_param }
    end

    it "responds with 200 OK" do
      get :show, params: { id: user1.to_param }
      expect(response.status).to eq 200
    end
  end

  describe "DELETE #destroy" do
    let(:user1) { FactoryBot.create(:user) }
    let(:user2) { FactoryBot.create(:user, :with_unique_params) }
    let!(:follow) do
      FactoryBot.create(:follow, source_id: user1.to_param, destination_id: user2.to_param)
    end

    it "deletes follow" do
      sign_in_user(user1)

      expect do
        delete :destroy, params: { id: follow.to_param }
      end.to change(Follow, :count).by(-1)
    end
  end
end
