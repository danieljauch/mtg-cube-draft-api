require "rails_helper"

RSpec.describe UsersController, type: :controller do
  describe "GET #search" do
    context "without a search query" do
      before { get :search }

      it "responds with 200 OK" do
        expect(response.status).to eq 200
        expect(response.body).to eq "[]"
      end
    end

    context "with a search query" do
      let!(:user) { FactoryBot.create(:user) }

      before { get :search, params: { query: "e" } }

      it "responds with 200 OK" do
        expect(response.status).to eq 200
        expect(response.body).not_to eq "[]"
      end
    end
  end

  describe "GET #show" do
    let!(:user1) { FactoryBot.create(:user) }

    before { sign_in_user(user1) }

    context "when viewing your own profile" do
      xit "is OK" do
        get :show
        expect(response.status).to eq 200
      end
    end

    context "when viewing someone else's profile" do
      let!(:user2) { FactoryBot.create(:user) }

      xit "is OK" do
        before { get :show, params: { format: "json", username: user2.username } }
        expect(response.status).to eq 200
      end
    end
  end
end
