class ApplicationResource < JSONAPI::Resource
  def context
    { current_user: current_user }
  end
end
