class UserResource < JSONAPI::Resource
  attributes :email, :username

  has_many :cards
  has_many :cubes
  has_many :decks
  has_many :follows
  has_many :followings

  before_save do
    @model.user_id = context[:current_user].id if @model.new_record?
  end

  # def confirmed?
  #   @model.confirmed?
  # end
end
