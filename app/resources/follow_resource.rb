class FollowResource < JSONAPI::Resource
  belongs_to :source
  belongs_to :destination

  delegate :username, to: :destination
end
