class UserPolicy
  attr_reader :current_user, :model

  def initialize(current_user, model)
    @current_user = current_user
    @user = model
  end

  def permitted_attributes
    return %i[username] unless @current_user == @user
    %i[username email cubes decks cards follows]
  end

  def search?
    true
  end

  def show?
    true
  end
end
