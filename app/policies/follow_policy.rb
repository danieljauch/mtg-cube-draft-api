class FollowPolicy
  attr_reader :current_user, :model

  def initialize(current_user, model)
    @current_user = current_user
    @follow = model
  end

  def index?
    return false if @current_user.nil?
    true
  end

  def show?
    return false if @current_user.nil?
    true
  end

  def create?
    return false if @current_user.nil?
    true
  end

  def destroy?
    return false if @current_user.nil?
    return false if @current_user.id != @follow.source_id
    true
  end
end
