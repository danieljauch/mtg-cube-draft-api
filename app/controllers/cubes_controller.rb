class CubesController < ApplicationController
  def index
    set_cubes
  end

  def show
    set_cube
  end

  def new
    @cube = Cube.new
  end

  def create
    cube = Cube.new(cube_params)
    return redirect_to cubes_url, notice: "Cube successfully created." if cube.save
    render :new
  end

  def edit
    set_cube
  end

  def update
    set_cube
    return redirect_to @cube, notice: "Cube successfully updated." if @cube.update(cube_params)
    render :edit
  end

  def destroy
    set_cube
    @cube.destroy
    redirect_to cubes_url, notice: "Cube successfully deleted."
  end

  def draft
    set_cube
  end

  private

  def set_cubes
    @cubes = Cube.all
  end

  def set_cube
    @cube = current_user.cubes.find(params[:id])
  end

  def cube_params
    params.require(:cube).permit(:name, :category)
  end
end
