class UsersController < ApplicationController
  def search
    render json: User.search(params[:search], current_user, params[:limit])
  end

  def show
    set_user
    render json: @user.to_json
  end

  private

  def set_user
    return @user = User.where(username: params[:username]).first if params[:username]

    @user = current_user
  end

  def user_params
    params.require(:user).permit(:id, :username, :email, :password, :password_confirmation)
  end
end
