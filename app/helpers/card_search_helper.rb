module CardSearchHelper
  def self.find_by_id(id)
    MTG::Card.find(id)
  end

  def self.search_by_name(query)
    MTG::Card.where(name: query).all
  end
end
