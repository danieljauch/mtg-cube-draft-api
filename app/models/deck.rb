class Deck < ApplicationRecord
  belongs_to :cube

  has_many :cards, dependent: :destroy
end
