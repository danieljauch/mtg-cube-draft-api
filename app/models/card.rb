class Card < ApplicationRecord
  has_one :mtg_card, class_name: "MTG::Card", dependent: :destroy

  acts_as_taggable
end
