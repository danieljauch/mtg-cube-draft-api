class User < ApplicationRecord
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable,
         :validatable
  # :confirmable, :omniauthable

  attr_writer :login

  has_many :cubes, dependent: :destroy
  has_many :cards, dependent: :destroy
  has_many :decks, through: :cubes, dependent: :destroy
  has_many :follows, dependent: :destroy, inverse_of: :source, foreign_key: :source_id
  has_many :followings, through: :follows, source: :destination

  acts_as_taggable_on :used_tags

  validates :username,
            presence: true,
            uniqueness: { message: "taken" },
            format: {
              with: /\A[a-z0-9_.-]{8,24}\z/i,
              message: "can only use letters, numbers, and the symbols . - and _"
            },
            length: { in: 8..24, message: "must be between 8 and 24 characters" }

  validates :email, presence: true, uniqueness: { case_sensitive: false }

  validates :password,
            format: {
              with: /\A(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[\x20-\x7E]{8,24}\z/,
              message: "requires a lowercase letter, an uppercase letter, one digit, and a special character" # rubocop:disable Metrics/LineLength
            },
            length: { in: 8..24, message: "must be between 8 and 24 characters" }

  def self.search(query, current_user = nil, limit = 10)
    users = if query != ""
              User.where("username like :search OR email like :search", search: "%#{query}%")
            else
              User.order(:username)
            end

    users = users.reject { |user| user.id == current_user.id } if current_user

    return users[0..limit.to_i - 1] if users.count > limit.to_i

    users
  end

  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    conditions[:email].downcase! if conditions[:email]
    if conditions.key?(:login)
      find_by(["username = :value OR lower(email) = lower(:value)", { value: conditions[:login] }])
    elsif conditions.key?(:username) || conditions.key?(:email)
      find_by(conditions.to_h)
    end
  end

  def login
    @login || self.username || self.email # rubocop:disable Style/RedundantSelf
  end
end
