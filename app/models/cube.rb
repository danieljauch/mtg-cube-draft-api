class Cube < ApplicationRecord
  belongs_to :user

  has_many :cards, dependent: :destroy
  has_many :decks, dependent: :destroy

  acts_as_taggable_on :cube_tags
end
